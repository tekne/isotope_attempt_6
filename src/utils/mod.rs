/*!
Utilities used throughout isotope
*/
use std::rc::Rc;
use std::sync::Arc;

/// An object which is either borrowed or held in a smart (or not) pointer of some sort
#[derive(Debug)]
pub enum Bore<'ctx, T, R> where T: ?Sized {
    Borrowed(&'ctx T),
    Ref(R)
}

impl<'ctx, T, R> Clone for Bore<'ctx, T, R> where T: ?Sized, R: Clone {
    fn clone(&self) -> Self { match self {
        Bore::Borrowed(b) => Bore::Borrowed(b),
        Bore::Ref(r) => Bore::Ref(r.clone())
    } }
}

impl<'ctx, T, R> Copy for Bore<'ctx, T, R> where T: ?Sized, R: Copy {}

impl<'ctx, T, R> PartialEq for Bore<'ctx, T, R> where T: ?Sized + PartialEq, R: AsRef<T> {
    fn eq(&self, other : &Self) -> bool { self.as_ref().eq(other.as_ref()) }
}

impl<'ctx, T, R> Eq for Bore<'ctx, T, R> where T: ?Sized + Eq, R: AsRef<T> {}

impl<'ctx, T, R> From<&'ctx T> for Bore<'ctx, T, R> where T: ?Sized {
    fn from(b : &'ctx T) -> Self { Bore::Borrowed(b) }
}

impl<'ctx, T, R> AsRef<T> for Bore<'ctx, T, R> where T: ?Sized, R: AsRef<T> {
    fn as_ref(&self) -> &T { match self { Bore::Borrowed(b) => b, Bore::Ref(r) => r.as_ref() } }
}

impl<'ctx, T> From<Rc<T>> for Bore<'ctx, T, Rc<T>> where T: ?Sized {
    fn from(r : Rc<T>) -> Self { Bore::Ref(r) }
}

/// An object which is either borrowed or (atomically) reference counted
pub type Borc<'ctx, T> = Bore<'ctx, T, Arc<T>>;

impl<'ctx, T> From<Arc<T>> for Bore<'ctx, T, Arc<T>> where T: ?Sized {
    fn from(r : Arc<T>) -> Self { Bore::Ref(r) }
}
