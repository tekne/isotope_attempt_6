use std::num::NonZeroUsize;

pub mod function;
pub mod closure;
pub mod primitive;

/// A typing universe
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Universe(NonZeroUsize);

impl Universe {
    /// The universe of simple values
    #[inline(always)]
    pub fn set() -> Universe { Universe(NonZeroUsize::new(1).unwrap()) }
    /// The universe containing this universe
    #[inline(always)]
    pub fn next(self) -> Universe { Universe(NonZeroUsize::new(self.0.get() + 1).unwrap()) }
}

/// A typing context
pub trait TypingContext : AsRef<Self> + AsRef<StaticContext> {}

/// The static typing context
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct StaticContext;
impl TypingContext for StaticContext {}
impl AsRef<StaticContext> for StaticContext {
    fn as_ref(&self) -> &StaticContext { &StaticContext }
}

/// The trait to be implemented for isotope types
pub trait ContextualType {
    /// The typing context this type requires
    type TypingContext : TypingContext;
    /// What typing level this type is contained in
    fn level_c(&self, ctx : &Self::TypingContext) -> Universe;
    /// Whether this type is a typing universe
    fn is_universe_c(&self, ctx : &Self::TypingContext) -> bool;
    /// Whether this type holds types. May not be a universe, e.g., the
    /// type of cartesian products of types holds types, but is not closed
    /// under sums and is hence not a universe. True for all universes.
    #[inline] fn holds_types_c(&self, ctx : &Self::TypingContext) -> bool {
        self.is_universe_c(ctx)
    }
}

impl<T, R> Type<R> for T where T: ContextualType, R: TypingContext + AsRef<T::TypingContext> {
    fn level(&self, ctx : &R) -> Universe { self.level_c(ctx.as_ref()) }
    fn is_universe(&self, ctx : &R) -> bool { self.is_universe_c(ctx.as_ref()) }
    fn holds_types(&self, ctx : &R) -> bool { self.holds_types_c(ctx.as_ref()) }
}

/// The trait implemented by all isotope types. `ContextualType` is usually the right
/// thing to implement, though
pub trait Type<C: TypingContext> {
    /// What typing level this type is contained in
    fn level(&self, ctx : &C) -> Universe;
    /// Whether this type is a typing universe
    fn is_universe(&self, ctx : &C) -> bool;
    /// Whether this type holds types. May not be a universe, e.g., the
    /// type of cartesian products of types holds types, but is not closed
    /// under sums and is hence not a universe. True for all universes.
    fn holds_types(&self, ctx : &C) -> bool;
}

/// The trait implemented by all static isotope types. Implement `Type<StaticContext>`, not this!
pub trait StaticType : Type<StaticContext> {
    /// Get the (static) type of this value
    #[inline] fn level_s(&self) -> Universe { self.level(&StaticContext) }
    /// Whether this type is a typing universe
    #[inline] fn is_universe_s(&self) -> bool { self.is_universe(&StaticContext) }
    /// Whether this type holds types. May not be a universe, e.g., the
    /// type of cartesian products of types holds types, but is not closed
    /// under sums and is hence not a universe. True for all universes.
    #[inline] fn holds_types_s(&self) -> bool { self.holds_types(&StaticContext) }
}

impl ContextualType for Universe {
    type TypingContext = StaticContext;
    #[inline(always)] fn level_c(&self, _ : &StaticContext) -> Universe { self.next() }
    #[inline(always)] fn is_universe_c(&self, _ : &StaticContext) -> bool { true }
}

/// The trait implemented by simple isotope types (set-like types). Automatically
/// implements StaticType
pub trait Set {}

impl<S: Set> ContextualType for S {
    type TypingContext = StaticContext;
    #[inline(always)] fn level_c(&self, _ : &StaticContext) -> Universe { Universe::set() }
    #[inline(always)] fn is_universe_c(&self, _ : &StaticContext) -> bool { false }
}
