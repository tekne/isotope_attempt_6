use super::{ContextualType, Universe};

/// A function type
#[derive(Debug)]
pub struct Function<T, A, R> {
    arguments : A,
    result : R,
    marker : std::marker::PhantomData<fn() -> T>
}

impl<T, A, R> Function<T, A, R> {
    /// Create a new function type with the given arguments and result type
    #[inline]
    pub fn new<AP: Into<A>, RP: Into<R>>(&self, args : AP, res : RP) -> Function<T, A, R> {
        Function {
            arguments : args.into(),
            result : res.into(),
            marker : std::marker::PhantomData
        }
    }
}

impl<T, A, R> Function<T, A, R> where
    T: ContextualType,
    A: AsRef<[T]>,
    R: AsRef<T> {
    /// Get the maximum level of any arguments to this function
    #[inline]
    pub fn arg_level(&self, ctx : &T::TypingContext) -> Universe {
        //TODO: think about how to handle an empty argument list
        self.arguments.as_ref().iter().map(|t| t.level_c(ctx)).max().unwrap_or(Universe::set())
    }
    /// Get the level of the result of this function
    #[inline]
    pub fn result_level(&self, ctx : &T::TypingContext) -> Universe {
        //TODO: think about how to handle an empty argument list
        self.result.as_ref().level_c(ctx)
    }
}


impl<T, A, R> ContextualType for Function<T, A, R> where
    T: ContextualType,
    A: AsRef<[T]>,
    R: AsRef<T> {
    type TypingContext = T::TypingContext;
    #[inline]
    fn level_c(&self, ctx : &T::TypingContext) -> Universe {
        std::cmp::max(self.arg_level(ctx), self.result_level(ctx))
    }
    #[inline(always)] fn is_universe_c(&self, _ctx : &T::TypingContext) -> bool { false }
    #[inline(always)] fn holds_types_c(&self, _ctx : &T::TypingContext) -> bool { false }
}
