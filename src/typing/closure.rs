use crate::utils::Borc;
use super::{ContextualType, Universe, function::Function};

/// Take the closure of a given type kind under the standard type formers
pub enum TypeClosure<T, S, SA> {
    Type(T),
    Function(Function<Self, SA, S>)
}

impl<T, S, SA> ContextualType for TypeClosure<T, S, SA> where
    T: ContextualType,
    S: AsRef<Self>,
    SA: AsRef<[Self]> {
    type TypingContext = T::TypingContext;

    fn level_c(&self, ctx : &T::TypingContext) -> Universe {
        match self {
            TypeClosure::Type(t) => t.level_c(ctx),
            TypeClosure::Function(f) => f.level_c(ctx)
        }
    }
    fn is_universe_c(&self, ctx : &T::TypingContext) -> bool {
        match self {
            TypeClosure::Type(t) => t.is_universe_c(ctx),
            _ => false
        }
    }
    fn holds_types_c(&self, ctx : &T::TypingContext) -> bool {
        match self {
            TypeClosure::Type(t) => t.holds_types_c(ctx),
            _ => false
        }
    }
}

/// A type closure which borrows, when possible, from a typing context
/// Note that, once a bug is fixed, this should become a self-referential typedef
#[repr(transparent)]
pub struct BorrowedClosureHelper<'tcx, T> {
    closure : TypeClosure<T, Borc<'tcx, Self>, Borc<'tcx, [Self]>>
}

impl<'tcx, T> AsRef<
    TypeClosure<T, Borc<'tcx, BorrowedClosureHelper<'tcx, T>>,
    Borc<'tcx, [BorrowedClosureHelper<'tcx, T>]>>
    > for BorrowedClosureHelper<'tcx, T> {
    fn as_ref(&self) ->
    &TypeClosure<T, Borc<'tcx, BorrowedClosureHelper<'tcx, T>>,
    Borc<'tcx, [BorrowedClosureHelper<'tcx, T>]>> {
        &self.closure
    }
}

impl<'tcx, T> AsRef<
    TypeClosure<T, Borc<'tcx, BorrowedClosureHelper<'tcx, T>>,
    Borc<'tcx, [BorrowedClosureHelper<'tcx, T>]>>
    > for Borc<'tcx, BorrowedClosureHelper<'tcx, T>> {
    fn as_ref(&self) ->
    &TypeClosure<T, Borc<'tcx, BorrowedClosureHelper<'tcx, T>>,
    Borc<'tcx, [BorrowedClosureHelper<'tcx, T>]>> {
        let bc : &BorrowedClosureHelper<'tcx, T> = self.as_ref();
        bc.as_ref()
    }
}

impl<'tcx, T> AsRef<
    [TypeClosure<T, Borc<'tcx, BorrowedClosureHelper<'tcx, T>>,
    Borc<'tcx, [BorrowedClosureHelper<'tcx, T>]>>]
    > for Borc<'tcx, [BorrowedClosureHelper<'tcx, T>]> {
    fn as_ref(&self) ->
    &[TypeClosure<T, Borc<'tcx, BorrowedClosureHelper<'tcx, T>>,
    Borc<'tcx, [BorrowedClosureHelper<'tcx, T>]>>]
    {
        let arr : &[BorrowedClosureHelper<'tcx, T>] = self.as_ref();
        // Cool because of #[repr(transparent)]
        unsafe { std::mem::transmute(arr) }
    }
}

impl<'tcx, T> ContextualType for BorrowedClosureHelper<'tcx, T> where T: ContextualType {
    type TypingContext = T::TypingContext;
    #[inline(always)] fn level_c(&self, ctx : &T::TypingContext)
    -> Universe { self.closure.level_c(ctx) }
    #[inline(always)] fn is_universe_c(&self, ctx : &T::TypingContext)
    -> bool { self.closure.is_universe_c(ctx) }
    #[inline(always)] fn holds_types_c(&self, ctx : &T::TypingContext)
    -> bool { self.closure.holds_types_c(ctx) }
}

pub type BorrowedClosure<'tcx, T> = TypeClosure<
    T, Borc<'tcx, BorrowedClosureHelper<'tcx, T>>,
    Borc<'tcx, [BorrowedClosureHelper<'tcx, T>]>
    >;
