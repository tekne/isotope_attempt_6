use super::Set;

/// The type of 8-bit bytes
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Byte;
impl Set for Byte {}

/// The type of lists of 8-bit bytes
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Bytes;
impl Set for Bytes {}

/// The type of UTF-8 strings
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Str;
impl Set for Str {}

/// Primitive isotope types
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Primitive {
    Byte,
    Bytes,
    Str
}
impl Set for Primitive {}
impl From<Byte> for Primitive { fn from(_ : Byte) -> Self { Primitive::Byte } }
impl From<Bytes> for Primitive { fn from(_ : Bytes) -> Self { Primitive::Bytes } }
impl From<Str> for Primitive { fn from(_ : Str) -> Self { Primitive::Str } }
