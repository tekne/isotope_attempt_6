use lazycell::AtomicLazyCell;

/// A function evaluation
pub struct Eval<F, A, FR, AR, CT> {
    function : FR,
    arguments : AR,
    cached_type : AtomicLazyCell<CT>,
    f_mark : std::marker::PhantomData<fn() -> F>,
    a_mark : std::marker::PhantomData<fn() -> A>
}

impl<F, A, FR, AR, CT> Eval<F, A, FR, AR, CT> {
    #[inline]
    pub fn new<FI: Into<FR>, AI: Into<AR>>(f : FI, a : AI) -> Self {
        Eval {
            function : f.into(),
            arguments : a.into(),
            cached_type : AtomicLazyCell::new(),
            f_mark : std::marker::PhantomData,
            a_mark : std::marker::PhantomData
        }
    }
}

impl<F, A, FR, AR, CT> Eval<F, A, FR, AR, CT> where
    FR: AsRef<F>,
    AR: AsRef<[A]> {
    #[inline] pub fn args(&self) -> impl Iterator<Item=&A> { self.arguments.as_ref().iter() }
    #[inline] pub fn func(&self) -> &F { self.function.as_ref() }
}
