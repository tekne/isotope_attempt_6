use crate::utils::Borc;
use crate::typing::{
    Type, StaticContext, TypingContext,
    primitive::{Primitive, Byte, Bytes, Str}
};

pub mod eval;

/// An isotope value requiring a typing context.
pub trait ContextualValue {
    /// The typing context required to interpret this value
    type TypingContext : TypingContext;
    /// The type of types this value can be
    type TypeKind : Type<Self::TypingContext>;
    /// Get the type of a given value
    fn get_type_c(&self, ctx : &Self::TypingContext) -> Self::TypeKind;
}


/// An object-safe trait implemented by all isotope values with a given type
/// kind `T` or a sub-kind of that kind. Should *not* be directly implemented.
/// Instead, implement `Value<C>`` for a given context `C`
pub trait Value<C, T> where C: TypingContext, T: Type<C> {
    /// Get the type of a given value
    fn get_type(&self, ctx : C) -> T;
}

// NOTE: this impl is duplicated specifically because otherwise there is an overflow.
// Maybe raise an issue...
impl<C, T, V> Value<C, T> for V where
    C: TypingContext + AsRef<V::TypingContext>,
    T: Type<C> + From<V::TypeKind>,
    V: ContextualValue {
    #[inline]
    fn get_type(&self, ctx : C) -> T { T::from(self.get_type_c(ctx.as_ref())) }
}

/// Simple isotope constants of the primitive types
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum SimpleConstant<'vcx> {
    Byte(u8),
    Bytes(Borc<'vcx, [u8]>),
    Str(Borc<'vcx, str>),
}

impl<'vcx> ContextualValue for SimpleConstant<'vcx> {
    type TypingContext = StaticContext;
    type TypeKind = Primitive;
    #[inline]
    fn get_type_c(&self, _ : &StaticContext) -> Primitive {
        use SimpleConstant::*;
        match self {
            Byte(_) => Primitive::Byte,
            Bytes(_) => Primitive::Bytes,
            Str(_) => Primitive::Str
        }
    }
}

impl ContextualValue for u8 {
    type TypingContext = StaticContext;
    type TypeKind = Byte;
    #[inline(always)]
    fn get_type_c(&self, _ : &StaticContext) -> Byte { Byte }
}

impl<'vcx> From<u8> for SimpleConstant<'vcx> {
    #[inline(always)]
    fn from(b : u8) -> Self { SimpleConstant::Byte(b) }
}

impl<'vcx> ContextualValue for &'vcx [u8] {
    type TypingContext = StaticContext;
    type TypeKind = Bytes;
    #[inline(always)]
    fn get_type_c(&self, _ : &StaticContext) -> Bytes { Bytes }
}

impl<'vcx> From<&'vcx [u8]> for SimpleConstant<'vcx> {
    #[inline(always)]
    fn from(a : &'vcx [u8]) -> Self { SimpleConstant::Bytes(Borc::Borrowed(a)) }
}

impl<'vcx> ContextualValue for &'vcx str {
    type TypingContext = StaticContext;
    type TypeKind = Str;
    #[inline(always)]
    fn get_type_c(&self, _ : &StaticContext) -> Str { Str }
}

impl<'vcx> From<&'vcx str> for SimpleConstant<'vcx> {
    #[inline(always)]
    fn from(a : &'vcx str) -> Self { SimpleConstant::Str(Borc::Borrowed(a)) }
}
